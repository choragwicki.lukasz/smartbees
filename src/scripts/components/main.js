import { environment } from "../environments/environment";

export const main = () => {
    const productsCart = document.querySelector('.summary-products');
    const partSum = document.querySelector('.part-sum');
    const inputDelivery = document.querySelectorAll('input[name="delivery"]');
    const inputPassword = document.querySelectorAll('input[type="password"]');
    const toPay = document.querySelector('.to-payx');
    const btnDiscount = document.querySelector('.btn-discount');
    const keyDiscount = document.querySelector('.key-discount');
    const btnCancel = document.querySelector('.btn-cancel');
    const createAccount = document.querySelector('#create-account');
    const passwordWrapper = document.querySelector('.password-wrapper');
    const btnAccept = document.querySelector('.btn-apply');
    const keyDiscountInput = document.querySelector('#key-discount');
    const btnStartLogin = document.querySelector('.btn-start-login');
    const modal = document.querySelector('.container-modal');
    const inputsPay = document.querySelectorAll('.method-pay');
    const iconPassword = document.querySelectorAll('.password-wrapper i.fa');
    let partSumValue = 0;
    let score = 0;

//shows the payment methods you have clicked on
    const paymentMethod = () => {
        Array.from(inputsPay).find(el => {
            el.addEventListener('change', () => {
                inputsPay.forEach(els => els.classList.add('no-active'));
                el.classList.remove('no-active');
            });
        });
    };
    paymentMethod();

    const getModalActive = () => {
      btnStartLogin.addEventListener('click', (e) => {
          e.preventDefault();
          modal.classList.add('container-modal--active');
      });
    };
    getModalActive();

//discount code support
    const getDiscount = () => {
        const discountValue = (value) => {
            partSumValue = partSumValue - (partSumValue * value);
            toPay.textContent = partSumValue.toFixed(2);
            btnAccept.disabled = true;
        };
        btnAccept.addEventListener('click', (e) => {
            if (keyDiscountInput.value === 'RABAT15') {
                discountValue(0.15);
            } else if (keyDiscountInput.value === 'SMARTBEES') {
                discountValue(0.20);
            }
        });
    };
    getDiscount();

    //shows inputs having the type password
    const createNewUser = () => createAccount.addEventListener('change', (e) => passwordWrapper.classList.toggle('no-active'));
    createNewUser();

    //returns the cost with delivery
    const deliveryMethodChanged = (el) => {
        const item = parseFloat(el.value);
        score = (item + partSumValue).toFixed(2);
        toPay.textContent = score;
    };

    deliveryMethodChanged(Array.from(inputDelivery).find(input => input.checked));

    //user checking
    const getLogin = () => {
        document.querySelector('#login-form').addEventListener('submit', (e) => {
            e.preventDefault();
            const [login, password] = document.querySelectorAll('.login-modal input');
            fetch(`${ environment.apiUrl }/users`)
                .then(res => res.json())
                .then(users => {
                    if (users.find(user => user.username === login.value && user.password === password.value)) {
                        modal.classList.remove('container-modal--active');
                    }
                });
        });
    };
    getLogin();

    const cancelDiscount = () => {
        btnCancel.addEventListener('click', (e) => {
            e.preventDefault();
            keyDiscount.classList.remove('key-discount--active');
        });
    };
    cancelDiscount();

    const showKeyDiscount = () => {
        btnDiscount.addEventListener('click', (e) => {
            e.preventDefault();
            keyDiscount.classList.add('key-discount--active');
        });
    };
    showKeyDiscount();

    const showPassword = () => {
        iconPassword.forEach(inputEl => {
            inputEl.addEventListener('click', () => {
                inputPassword.forEach(els => els.type = "text");
            });
        });
    };
    showPassword();

    const getSum = () => inputDelivery.forEach(el => el.addEventListener("change", deliveryMethodChanged.bind(this, el)));
    getSum();

    let cart = null;
    const getProducts = () => {
        fetch(`${environment.apiUrl}/carts/2`).then(res => res.json()).then(shoppingCart => {
            cart = shoppingCart;
            Promise.all(
                cart.products.map(({ productId }) => fetch(`${environment.apiUrl}/products/${ productId }`)
                    .then(res => res.json()))
            ).then(products => {
                products.forEach(product => {
                    const { quantity } = cart.products.find(el => el.productId === product.id);
                    productsCart.innerHTML += `
                                             <div class="summary-product">
                                                 <div class="summary-products-information">
                                                     <div class="img-products" class="form-img">
                                                         <img src="${ product.image }" class="form-img" alt="">
                                                     </div>
                                                     <div class="products-box">
                                                         <h3>${ product.title }</h3>
                                                         <h4>Ilość: ${ quantity }</h4>
                                                     </div>
                                                 </div>
                                               <div class="products-price">
                                                 <h3>${ (product.price * quantity).toFixed(2) }zł</h3>
                                               </div>
                                             </div>`;
                    partSumValue += product.price * quantity;
                    partSum.textContent = partSumValue.toFixed(2) + ' zł';
                    toPay.textContent = partSumValue.toFixed(2) + ' zł';
                });
            }).catch(err => err);
        });
    };
    getProducts();
};

